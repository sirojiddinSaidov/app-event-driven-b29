package uz.pdp.appeventdriven.event;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class SMSEvent extends ApplicationEvent {

    private final String phone;

    public SMSEvent(Object source, String phone) {
        super(source);
        this.phone = phone;
    }


}
