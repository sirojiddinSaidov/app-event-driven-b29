package uz.pdp.appeventdriven.event;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class OtherEvent extends ApplicationEvent {

    private final String phone;

    public OtherEvent(Object source, String phone) {
        super(source);
        this.phone = phone;
    }


}
