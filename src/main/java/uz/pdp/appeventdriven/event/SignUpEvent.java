package uz.pdp.appeventdriven.event;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class SignUpEvent extends ApplicationEvent {

    private final String phone;

    public SignUpEvent(Object source, String phone) {
        super(source);
        this.phone = phone;
    }


}
