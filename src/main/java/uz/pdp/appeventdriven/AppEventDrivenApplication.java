package uz.pdp.appeventdriven;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableAsync
public class AppEventDrivenApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppEventDrivenApplication.class, args);
    }

}
