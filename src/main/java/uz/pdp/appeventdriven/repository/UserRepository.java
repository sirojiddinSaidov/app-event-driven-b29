package uz.pdp.appeventdriven.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appeventdriven.collection.User;

public interface UserRepository extends JpaRepository<User, Integer> {
    boolean existsByPhone(String phone);
}
