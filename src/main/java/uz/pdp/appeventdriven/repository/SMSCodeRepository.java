package uz.pdp.appeventdriven.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appeventdriven.collection.SMSCode;

public interface SMSCodeRepository extends JpaRepository<SMSCode, Integer> {

}
