//package uz.pdp.appeventdriven.service;
//
//import lombok.RequiredArgsConstructor;
//import org.springframework.context.event.EventListener;
//import org.springframework.core.annotation.Order;
//import org.springframework.scheduling.annotation.Async;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.event.TransactionPhase;
//import org.springframework.transaction.event.TransactionalEventListener;
//import uz.pdp.appeventdriven.collection.SMSCode;
//import uz.pdp.appeventdriven.event.SignUpEvent;
//import uz.pdp.appeventdriven.repository.SMSCodeRepository;
//
//import java.util.Random;
//
//@Service
//@RequiredArgsConstructor
//public class SMSService {
//
//    private final SMSCodeRepository smsCodeRepository;
//
//
//        @Async
////    @Order(5)
//    @TransactionalEventListener(value = SignUpEvent.class,
//            phase = TransactionPhase.AFTER_COMMIT)
//    public void sendSMS(SignUpEvent signUpEvent) {
//        String code = String.valueOf(new Random().nextInt(10_000_000)).substring(0, 6);
//        sendSMSToOTPServer(signUpEvent.getPhone(), code);
//        SMSCode smsCode = new SMSCode(signUpEvent.getPhone(), code);
//        smsCodeRepository.save(smsCode);
//    }
//
//    //
////    @Order(10)
//    @Async
//    @TransactionalEventListener(value = SignUpEvent.class,
//            phase = TransactionPhase.BEFORE_COMMIT)
//    public void saveBla(SignUpEvent signUpEvent) {
//        System.out.println(signUpEvent);
//        if (true)
//            throw new RuntimeException("Save bla parot qildi. Transaction ni orqaga qaytar: " + Thread.currentThread().getName());
//        smsCodeRepository.save(new SMSCode("dasdas", "sadsad"));
//    }
//
//    @Async
//    //    @Order(20)
//    @EventListener
//    public void battar(SignUpEvent signUpEvent) {
//        System.out.println(Thread.currentThread().getName());
//        System.out.println("Battar STARTED: " + signUpEvent.getPhone());
//        try {
//            Thread.sleep(500);
//        } catch (InterruptedException e) {
//            throw new RuntimeException(e);
//        }
//        System.out.println("BAttar FINISHED: " + signUpEvent.getPhone());
//    }
//
//    private void sendSMSToOTPServer(String phone, String code) {
//        System.out.println(String.format("SMS sent via OTP phone %s, code %s", phone, code));
////        throw new RuntimeException(phone + "/" + code);
//    }
//
//}
