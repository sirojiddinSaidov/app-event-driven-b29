package uz.pdp.appeventdriven.service;

import lombok.RequiredArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;
import uz.pdp.appeventdriven.collection.SMSCode;
import uz.pdp.appeventdriven.event.OtherEvent;
import uz.pdp.appeventdriven.event.SMSEvent;
import uz.pdp.appeventdriven.event.SignUpEvent;
import uz.pdp.appeventdriven.repository.SMSCodeRepository;

import java.util.Random;

@Service
@RequiredArgsConstructor
public class SMSServiceSecond {

    private final SMSCodeRepository smsCodeRepository;


    @EventListener(value = SignUpEvent.class)
    public SMSEvent sendSMS(SignUpEvent signUpEvent) {
        System.out.println("SENS SMS ishladi");
        String code = String.valueOf(new Random().nextInt(10_000_000)).substring(0, 6);
        sendSMSToOTPServer(signUpEvent.getPhone(), code);
        SMSCode smsCode = new SMSCode(signUpEvent.getPhone(), code);
        smsCodeRepository.save(smsCode);
        return new SMSEvent(this, signUpEvent.getPhone());
    }


    @EventListener(value = SMSEvent.class)
    public OtherEvent saveBla(SMSEvent smsEvent) {
        System.out.println("Bla ishladi:");
        System.out.println(smsEvent.getPhone());
        return new OtherEvent(this, smsEvent.getPhone());
    }

    @EventListener(value = OtherEvent.class)
    public void battar(OtherEvent otherEvent) {
        System.out.println("Battar ishladi: " + otherEvent.getPhone());
    }

    @EventListener(value = OtherEvent.class)
    public void battar3(OtherEvent otherEvent) {
        System.out.println("Battar3 ishladi: " + otherEvent.getPhone());
    }

    private void sendSMSToOTPServer(String phone, String code) {
        System.out.println(String.format("SMS sent via OTP phone %s, code %s", phone, code));
//        throw new RuntimeException(phone + "/" + code);
    }

}
