package uz.pdp.appeventdriven.service;

import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import uz.pdp.appeventdriven.collection.User;
import uz.pdp.appeventdriven.event.SignUpEvent;
import uz.pdp.appeventdriven.payload.RegisterDTO;
import uz.pdp.appeventdriven.repository.UserRepository;

import java.util.Objects;

@Service
@RequiredArgsConstructor
public class AuthService {

    private final UserRepository userRepository;
    private final ApplicationEventPublisher eventPublisher;
//    private final SMSService smsService;

    @Transactional
    public void signUp(RegisterDTO registerDTO) {
        checkPasswordsEquality(registerDTO.password(), registerDTO.prePassword());

        if (userRepository.existsByPhone(registerDTO.phone()))
            throw new RuntimeException("Already registered");

        userRepository.save(User.builder()
                .password(registerDTO.password())
                .phone(registerDTO.phone())
                .build());

//        SignUpEvent event = new SignUpEvent(this, registerDTO.phone());
        eventPublisher.publishEvent(new SignUpEvent(this, registerDTO.phone()));
        System.out.println("SIGN UP FINISHED: " + Thread.currentThread().getName());
    }

    private void checkPasswordsEquality(String password, String prePassword) {
        if (!Objects.equals(password, prePassword))
            throw new RuntimeException("Passwords not match");
    }
}
